package com.training.bankapplication.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
 
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
 
import com.training.bankapplication.dto.LoginDto;
import com.training.bankapplication.entity.Account;
import com.training.bankapplication.service.CustomerService;
 
@ExtendWith(SpringExtension.class)
class LoginControllerTest {
 
	
	@InjectMocks
	LoginController loginController;
	@Mock
	CustomerService customerService;

	 @Test
	    void testAddUser_Positive() {
		 LoginDto loginDto=new LoginDto();
	        Account expectedResponse = new Account();
	        when(customerService.login(loginDto)).thenReturn(expectedResponse);

	        ResponseEntity<Account> responseEntity =loginController.userLogin(loginDto) ;
	        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
	        assertEquals(expectedResponse, responseEntity.getBody());
	    }
}
