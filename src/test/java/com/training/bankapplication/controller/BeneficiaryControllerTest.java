package com.training.bankapplication.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.bankapplication.dto.BeneficiaryDto;
import com.training.bankapplication.dto.BeneficiaryResponseDto;
import com.training.bankapplication.service.BeneficiaryService;

@ExtendWith(SpringExtension.class)
class BeneficiaryControllerTest {
	@InjectMocks
	BeneficiaryController beneficiaryController;
	@Mock
	BeneficiaryService  beneficiaryService;

	 @Test
	    void testAddUser_Positive() {
	        BeneficiaryDto beneficiaryDto=new BeneficiaryDto();
	        BeneficiaryResponseDto beneficiaryResponseDto=new BeneficiaryResponseDto();
	        when(beneficiaryService.addBeneficiary(beneficiaryDto)).thenReturn(beneficiaryResponseDto);
	        ResponseEntity<BeneficiaryResponseDto> responseEntity =beneficiaryController.addBeneficiary(beneficiaryDto);
	        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
	        assertEquals(beneficiaryResponseDto, responseEntity.getBody());
	    }
}
