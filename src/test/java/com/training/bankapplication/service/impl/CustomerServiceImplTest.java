package com.training.bankapplication.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.bankapplication.dto.CustomerDto;
import com.training.bankapplication.dto.LoginDto;
import com.training.bankapplication.dto.ResponseDto;
import com.training.bankapplication.entity.Account;
import com.training.bankapplication.entity.BeneficiaryCustomer;
import com.training.bankapplication.entity.Customer;
import com.training.bankapplication.exception.CustomerAlreadyExists;
import com.training.bankapplication.exception.UnAuthorizedAccessException;
import com.training.bankapplication.repository.AccountRepository;
import com.training.bankapplication.repository.BeneficiaryCustomerRepository;
import com.training.bankapplication.repository.CustomerRepository;
 
@ExtendWith(SpringExtension.class)
class CustomerServiceImplTest {
 
	@Mock
	BeneficiaryCustomerRepository beneficiaryCustomerRepository;
 
	@Mock
	private CustomerRepository customerRepository;
 
	@Mock
	private AccountRepository accountRepository;
 
	@InjectMocks
	private CustomerServiceImpl customerService;
 
	@Test
	 void testAddCustomer_Success() {
 
		CustomerDto customerDto = new CustomerDto();
		Customer customer = new Customer();
		customerDto.setAadharNumber("897657890987");
		customerDto.setContactNo("8088010687");
		customerDto.setCustomerName("divya");
		customerDto.setDateOfBirth(LocalDate.parse("2001-07-25"));
		customerDto.setEmailId("divya@gmail.com");
		customerDto.setPassword("Divya@514");
		Account account = new Account();
		BeneficiaryCustomer beneficiaryCustomer = new BeneficiaryCustomer();
		beneficiaryCustomer.setCustomer(customer);
		Mockito.when(beneficiaryCustomerRepository.save(beneficiaryCustomer)).thenReturn(beneficiaryCustomer);
		Mockito.when(accountRepository.save(account)).thenReturn(account);
		BeanUtils.copyProperties(customerDto, customer);
		customerRepository.save(customer);
 
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		ResponseDto responseDto = customerService.addCustomer(customerDto);
		assertNotNull(responseDto);
		assertEquals("Customer Details added and Account created", responseDto.getMessage());
 
	}
 
	@Test
	void addUser_UserAlreadyExists() {
 
		Customer customer = Customer.builder().emailId("divya@gmail.com").build();
 
		CustomerDto customerDto = CustomerDto.builder().emailId("divya@gmail.com").build();
 
		Mockito.when(customerRepository.findByEmailId(customerDto.getEmailId())).thenReturn(Optional.of(customer));
 
		CustomerAlreadyExists exception = assertThrows(CustomerAlreadyExists.class,
				() -> customerService.addCustomer(customerDto));
 
		assertEquals("Customer already exists", exception.getMessage());
 
	}
 
	@Test
	void login_PositiveTest() {
 
		Customer customer = Customer.builder().emailId("divya@gmail.com").password("Divya@514").build();
 
		LoginDto loginDto = LoginDto.builder().emailId("divya@gmail.com").password("Divya@514").build();
		Mockito.when(customerRepository.findByemailIdAndPassword(loginDto.getEmailId(), loginDto.getPassword()))
				.thenReturn(customer);
		Account response = customerService.login(loginDto);
		System.out.println(response);
		assertEquals(response, customer.getAccount());
 
	}
	@Test
	void login_NegativeTest_UserNotFound() {
		LoginDto loginDto = new LoginDto();
		loginDto.setEmailId("divya.com");
		loginDto.setPassword("divya14");
		when(customerRepository.findByemailIdAndPassword(loginDto.getEmailId(), loginDto.getPassword())).thenReturn(null);
 
		UnAuthorizedAccessException exception = assertThrows(UnAuthorizedAccessException.class,
				() -> customerService.login(loginDto));
		assertEquals("Invalid Credentials", exception.getMessage());
 
	}
 
	
 
}
