package com.training.bankapplication.exception;

public class ZeroBalanceException extends RuntimeException{


	private static final long serialVersionUID = 1L;
	public ZeroBalanceException() {
		super("Zero Balance Exception");
	}
	public ZeroBalanceException(String msg) {
		super(msg);
	}

}
