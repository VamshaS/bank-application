package com.training.bankapplication.exception;

public class UnAuthorizedAccessException extends RuntimeException {
	 
	private static final long serialVersionUID = 1L;
 
	public UnAuthorizedAccessException(String message) {
		super(message);
	}
 
	public UnAuthorizedAccessException() {
		super("Unauthorized Access");
	}
 
}
