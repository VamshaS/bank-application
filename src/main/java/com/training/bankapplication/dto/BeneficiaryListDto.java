package com.training.bankapplication.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BeneficiaryListDto {
	@NotBlank(message = "Please provide beneficiary Name")
	private String beneficiaryName;
	private long accountNumber;
	@NotBlank(message = "ifsc is required")
	private String iFSC;
	
}
