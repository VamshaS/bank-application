package com.training.bankapplication.dto;

import java.time.LocalDate;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerDto {

	@NotBlank(message = "customerName is required")
	private String customerName;

	@NotBlank(message = "emailId is required")
	@Email
	private String emailId;

	
	private LocalDate dateOfBirth;

	@NotBlank(message = "contactNo is required")
	@Pattern(regexp = "[6-9]\\d{9}")
	private String contactNo;

	@NotBlank(message = "password is required")
	private String password;

	@NotBlank(message = "aadharNumber is required")
	@Pattern(regexp = "\\d{12}")
	private String aadharNumber;

}
