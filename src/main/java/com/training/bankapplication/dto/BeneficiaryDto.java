package com.training.bankapplication.dto;

import java.util.List;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BeneficiaryDto {
	private long accountNumber;
	@Valid
	private List<BeneficiaryListDto> beneficiaryListDtos;
}
