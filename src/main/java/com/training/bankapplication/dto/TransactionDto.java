package com.training.bankapplication.dto;

import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDto {
	private long fromAccountNumber;
	private long toAccountNumber;
	@Min(value = 50)
	private double amount;
}
