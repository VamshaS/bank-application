package com.training.bankapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.bankapplication.entity.BeneficiaryCustomer;
import com.training.bankapplication.entity.Customer;

public interface BeneficiaryCustomerRepository extends JpaRepository<BeneficiaryCustomer, Long> {
	BeneficiaryCustomer findByCustomer(Customer customer);
}
