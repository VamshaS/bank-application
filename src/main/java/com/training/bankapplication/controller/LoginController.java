package com.training.bankapplication.controller;
 
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.training.bankapplication.dto.LoginDto;
import com.training.bankapplication.entity.Account;
import com.training.bankapplication.service.CustomerService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
 
@RestController
@RequestMapping("/")
@AllArgsConstructor
public class LoginController {
	
	CustomerService customerService;
	@PutMapping("/login")
	public ResponseEntity<Account> userLogin(@Valid @RequestBody LoginDto loginDto) {
		return new ResponseEntity<>(customerService.login(loginDto), HttpStatus.CREATED);
	}
 
}