package com.training.bankapplication.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.training.bankapplication.dto.BeneficiaryDto;
import com.training.bankapplication.dto.BeneficiaryResponseDto;
import com.training.bankapplication.service.BeneficiaryService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("/beneficiaries")
public class BeneficiaryController {
	private BeneficiaryService beneficiaryService;
	@PostMapping
	public ResponseEntity<BeneficiaryResponseDto> addBeneficiary(@RequestBody @Valid BeneficiaryDto beneficiaryDto){
		return new ResponseEntity<>(beneficiaryService.addBeneficiary(beneficiaryDto),HttpStatus.CREATED);
	}
	
}
