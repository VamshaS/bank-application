package com.training.bankapplication.controller;
 
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.training.bankapplication.dto.CustomerDto;
import com.training.bankapplication.dto.ResponseDto;
import com.training.bankapplication.service.CustomerService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
 
@RestController
@RequestMapping("/")
@AllArgsConstructor
public class CustomerController {
	
	CustomerService customerService;
	@PostMapping("/customers")
	public ResponseEntity<ResponseDto> addUser(@RequestBody @Valid  CustomerDto customerDto) {
		return new ResponseEntity<>(customerService.addCustomer(customerDto), HttpStatus.CREATED);
	}

 
}
