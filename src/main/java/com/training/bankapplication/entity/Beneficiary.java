package com.training.bankapplication.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Beneficiary {

	@Id//chaitrapshetty


	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long beneficiaryId;
	private String beneficiaryName;
	private long accountNumber;
	private String iFSC;
	private long beneficiaryCustomerId;

}
