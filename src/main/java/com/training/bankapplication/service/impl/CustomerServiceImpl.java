package com.training.bankapplication.service.impl;

import java.security.SecureRandom;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.training.bankapplication.dto.CustomerDto;
import com.training.bankapplication.dto.LoginDto;
import com.training.bankapplication.dto.ResponseDto;
import com.training.bankapplication.entity.Account;
import com.training.bankapplication.entity.BeneficiaryCustomer;
import com.training.bankapplication.entity.Customer;
import com.training.bankapplication.entity.LoginStatus;
import com.training.bankapplication.exception.CustomerAlreadyExists;
import com.training.bankapplication.exception.UnAuthorizedAccessException;
import com.training.bankapplication.repository.AccountRepository;
import com.training.bankapplication.repository.BeneficiaryCustomerRepository;
import com.training.bankapplication.repository.CustomerRepository;
import com.training.bankapplication.service.CustomerService;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
 
@Service
@Slf4j
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService{
	CustomerRepository customerRepository;
	AccountRepository accountRepository;
	BeneficiaryCustomerRepository beneficiaryCustomerRepository;
	
	public ResponseDto addCustomer(@Valid CustomerDto customerDto)
	{
		Optional <Customer> customer=customerRepository.findByEmailId(customerDto.getEmailId());
		if(customer.isPresent())
		{
			log.error("Customer already exists");
			throw new CustomerAlreadyExists("Customer already exists");
		}
		Account account=new Account();
		SecureRandom secureRandom=new SecureRandom();
		account.setAccountNumber(secureRandom.nextLong(999999-100000+1L)+1);
		account.setAccountType("savings");
		account.setBalance(1000.00);
		accountRepository.save(account);
		Customer newCustomer=new Customer();
		BeneficiaryCustomer beneficiaryCustomer=new BeneficiaryCustomer();
		beneficiaryCustomer.setCustomer(newCustomer);
		beneficiaryCustomerRepository.save(beneficiaryCustomer);
		newCustomer.setAccount(account);
		
         BeanUtils.copyProperties(customerDto, newCustomer);
    
         customerRepository.save(newCustomer);
         log.info("Customer Details added and Account created");
		return  new ResponseDto("Customer Details added and Account created");
 
 
		
	}

	public Account login(LoginDto loginDto) {
 
		Customer customer = customerRepository.findByemailIdAndPassword(loginDto.getEmailId(), loginDto.getPassword());
		if (customer != null) {
			customer.setLoginStatus(LoginStatus.LOGIN);
		 customerRepository.save(customer);
		} else {
			log.error("Invalid Credentials or user Not found");
			throw new UnAuthorizedAccessException("Invalid Credentials");
		}
		log.info("User Logged in successfully");
		return customer.getAccount();
	}
 
}