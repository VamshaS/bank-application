package com.training.bankapplication.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.training.bankapplication.dto.TransactionDto;
import com.training.bankapplication.dto.TransactionResponseDto;
import com.training.bankapplication.entity.Account;
import com.training.bankapplication.entity.Beneficiary;
import com.training.bankapplication.entity.BeneficiaryCustomer;
import com.training.bankapplication.entity.Customer;
import com.training.bankapplication.entity.LoginStatus;
import com.training.bankapplication.entity.Transaction;
import com.training.bankapplication.exception.AccountNotFoundException;
import com.training.bankapplication.exception.CustomerNotLoggedInException;
import com.training.bankapplication.exception.ZeroBalanceException;
import com.training.bankapplication.repository.AccountRepository;
import com.training.bankapplication.repository.BeneficiaryCustomerRepository;
import com.training.bankapplication.repository.BeneficiaryRepository;
import com.training.bankapplication.repository.CustomerRepository;
import com.training.bankapplication.repository.TransactionRepository;
import com.training.bankapplication.service.TransactionService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Service
@AllArgsConstructor
@Slf4j
public class TransactionServiceImpl implements TransactionService {
	private AccountRepository accountRepository;
	private CustomerRepository customerRepository;
	private BeneficiaryCustomerRepository beneficiaryCustomerRepository;
	private BeneficiaryRepository beneficiaryRepository;
	private TransactionRepository transactionRepository;
	@Override
	public TransactionResponseDto fundTransfer(TransactionDto transactionDto) {
		long accountNumber=transactionDto.getFromAccountNumber();
		long beneficiaryAccountNumber=transactionDto.getToAccountNumber();
		Account account=accountRepository.findByAccountNumber(accountNumber).orElseThrow(AccountNotFoundException::new);
		Customer customer=customerRepository.findByAccount(account);
		if(customer.getLoginStatus().equals(LoginStatus.LOGIN)) {
		if((account.getBalance()-transactionDto.getAmount())>=0) {
		
		BeneficiaryCustomer beneficiaryCustomer=beneficiaryCustomerRepository.findByCustomer(customer);
		List<Beneficiary> beneficiaries=beneficiaryRepository.findByBeneficiaryCustomerId(beneficiaryCustomer.getBeneficiaryCustomerId());		
		Beneficiary myBeneficiary=beneficiaries.stream().filter(beneficiary->beneficiary.getAccountNumber()== beneficiaryAccountNumber).findFirst().orElseThrow(AccountNotFoundException::new);
		Transaction transaction=new Transaction();
		BeanUtils.copyProperties(transactionDto, transaction);
		transaction.setToAccountNumber(myBeneficiary.getAccountNumber());
		transaction.setDate(LocalDate.now());
	   
		account.setBalance(account.getBalance()-transactionDto.getAmount());
		 accountRepository.save(account);
		 transactionRepository.save(transaction);
		log.info("Fund transfer successful");
		return new TransactionResponseDto("Fund transfer successful", HttpStatus.CREATED.value());
		}
		log.error("Check balance and try again");
		throw new ZeroBalanceException("Check balance and try again");
		}
		log.error("Customer Not LoggedIn Exception");
		throw new CustomerNotLoggedInException();
	}

}
