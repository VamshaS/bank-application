package com.training.bankapplication.service;

import com.training.bankapplication.dto.BeneficiaryDto;
import com.training.bankapplication.dto.BeneficiaryResponseDto;

public interface BeneficiaryService {
	BeneficiaryResponseDto addBeneficiary(BeneficiaryDto beneficiaryDto);
}
