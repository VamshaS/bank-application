package com.training.bankapplication.service;

import com.training.bankapplication.dto.CustomerDto;
import com.training.bankapplication.dto.LoginDto;
import com.training.bankapplication.dto.ResponseDto;
import com.training.bankapplication.entity.Account;
 
public interface CustomerService {
 
	ResponseDto addCustomer(CustomerDto customerDto);
	Account login(LoginDto loginDto);
}